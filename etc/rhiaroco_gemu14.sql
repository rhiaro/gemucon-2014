-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2013 at 01:51 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rhiaroco_gemu14`
--

-- --------------------------------------------------------

--
-- Table structure for table `perch2_content_index`
--

CREATE TABLE IF NOT EXISTS `perch2_content_index` (
  `indexID` int(10) NOT NULL AUTO_INCREMENT,
  `itemID` int(10) NOT NULL DEFAULT '0',
  `regionID` int(10) NOT NULL DEFAULT '0',
  `pageID` int(10) NOT NULL DEFAULT '0',
  `itemRev` int(10) NOT NULL DEFAULT '0',
  `indexKey` char(64) NOT NULL DEFAULT '-',
  `indexValue` char(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`indexID`),
  KEY `idx_key` (`indexKey`),
  KEY `idx_val` (`indexValue`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_item` (`itemID`),
  KEY `idx_keyval` (`indexKey`,`indexValue`),
  KEY `idx_regrev` (`regionID`,`itemRev`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `perch2_content_index`
--

INSERT INTO `perch2_content_index` (`indexID`, `itemID`, `regionID`, `pageID`, `itemRev`, `indexKey`, `indexValue`) VALUES
(10, 1, 1, 1, 1, '_order', '1000'),
(9, 1, 1, 1, 1, '_id', '1'),
(8, 1, 1, 1, 1, 'text_processed', '<p>This is the intro</p>'),
(7, 1, 1, 1, 1, 'text_raw', 'This is the intro'),
(6, 1, 1, 1, 1, 'text', '<p>This is the intro</p>'),
(24, 2, 4, 2, 2, '_order', '1000'),
(23, 2, 4, 2, 2, '_id', '2'),
(22, 2, 4, 2, 2, 'date', '2013-06-30'),
(21, 2, 4, 2, 2, 'newstext_processed', '<p>Test news</p>'),
(20, 2, 4, 2, 2, 'newstext_raw', 'Test news'),
(19, 2, 4, 2, 2, 'newstext', '<p>Test news</p>'),
(18, 2, 4, 2, 2, 'newstitle', 'Test'),
(25, 3, 4, 2, 2, 'newstitle', 'Test 2'),
(26, 3, 4, 2, 2, 'newstext', '<p>Another test news</p>'),
(27, 3, 4, 2, 2, 'newstext_raw', 'Another test news'),
(28, 3, 4, 2, 2, 'newstext_processed', '<p>Another test news</p>'),
(29, 3, 4, 2, 2, 'date', '2013-06-30'),
(30, 3, 4, 2, 2, '_id', '3'),
(31, 3, 4, 2, 2, '_order', '1001'),
(45, 4, 5, 2, 1, '_order', '1000'),
(44, 4, 5, 2, 1, '_id', '4'),
(43, 4, 5, 2, 1, 'date', '2013-06-30'),
(42, 4, 5, 2, 1, 'newstext_processed', '<p>TEst news</p>'),
(41, 4, 5, 2, 1, 'newstext_raw', 'TEst news'),
(40, 4, 5, 2, 1, 'newstext', '<p>TEst news</p>'),
(39, 4, 5, 2, 1, 'newstitle', 'Test'),
(46, 8, 19, 3, 1, 'boxtitle', 'FAQ'),
(47, 8, 19, 3, 1, 'text', '<p>Content</p>'),
(48, 8, 19, 3, 1, 'text_raw', 'Content'),
(49, 8, 19, 3, 1, 'text_processed', '<p>Content</p>'),
(50, 8, 19, 3, 1, '_id', '8'),
(51, 8, 19, 3, 1, '_order', '1000'),
(52, 9, 18, 4, 1, 'boxtitle', 'Venue'),
(53, 9, 18, 4, 1, 'text', '<p>Information</p>'),
(54, 9, 18, 4, 1, 'text_raw', 'Information'),
(55, 9, 18, 4, 1, 'text_processed', '<p>Information</p>'),
(56, 9, 18, 4, 1, '_id', '9'),
(57, 9, 18, 4, 1, '_order', '1000'),
(69, 10, 8, 5, 2, '_order', '1000'),
(68, 10, 8, 5, 2, '_id', '10'),
(67, 10, 8, 5, 2, 'text_processed', '<p>Intro</p>'),
(66, 10, 8, 5, 2, 'text_raw', 'Intro'),
(65, 10, 8, 5, 2, 'text', '<p>Intro</p>'),
(64, 10, 8, 5, 2, 'boxtitle', 'Events'),
(70, 11, 8, 5, 2, 'boxtitle', 'An Event'),
(71, 11, 8, 5, 2, 'text', '<p>Information</p>'),
(72, 11, 8, 5, 2, 'text_raw', 'Information'),
(73, 11, 8, 5, 2, 'text_processed', '<p>Information</p>'),
(74, 11, 8, 5, 2, '_id', '11'),
(75, 11, 8, 5, 2, '_order', '1001'),
(76, 12, 9, 6, 1, 'boxtitle', 'Gaming'),
(77, 12, 9, 6, 1, 'text', '<p>Information</p>'),
(78, 12, 9, 6, 1, 'text_raw', 'Information'),
(79, 12, 9, 6, 1, 'text_processed', '<p>Information</p>'),
(80, 12, 9, 6, 1, '_id', '12'),
(81, 12, 9, 6, 1, '_order', '1000'),
(82, 13, 10, 7, 1, 'boxtitle', 'Accommodation'),
(83, 13, 10, 7, 1, 'text', '<p>Informatation</p>'),
(84, 13, 10, 7, 1, 'text_raw', 'Informatation'),
(85, 13, 10, 7, 1, 'text_processed', '<p>Informatation</p>'),
(86, 13, 10, 7, 1, '_id', '13'),
(87, 13, 10, 7, 1, '_order', '1000'),
(88, 14, 11, 8, 1, 'boxtitle', 'Guests'),
(89, 14, 11, 8, 1, 'text', '<p>Intro</p>'),
(90, 14, 11, 8, 1, 'text_raw', 'Intro'),
(91, 14, 11, 8, 1, 'text_processed', '<p>Intro</p>'),
(92, 14, 11, 8, 1, '_id', '14'),
(93, 14, 11, 8, 1, '_order', '1000'),
(94, 15, 12, 9, 1, 'boxtitle', 'Cosplay'),
(95, 15, 12, 9, 1, 'text', '<p>Information</p>'),
(96, 15, 12, 9, 1, 'text_raw', 'Information'),
(97, 15, 12, 9, 1, 'text_processed', '<p>Information</p>'),
(98, 15, 12, 9, 1, '_id', '15'),
(99, 15, 12, 9, 1, '_order', '1000'),
(100, 16, 13, 10, 1, 'boxtitle', 'Charity'),
(101, 16, 13, 10, 1, 'text', '<p>Information</p>'),
(102, 16, 13, 10, 1, 'text_raw', 'Information'),
(103, 16, 13, 10, 1, 'text_processed', '<p>Information</p>'),
(104, 16, 13, 10, 1, '_id', '16'),
(105, 16, 13, 10, 1, '_order', '1000'),
(106, 17, 14, 11, 1, 'boxtitle', 'Conbook'),
(107, 17, 14, 11, 1, 'text', '<p>Information</p>'),
(108, 17, 14, 11, 1, 'text_raw', 'Information'),
(109, 17, 14, 11, 1, 'text_processed', '<p>Information</p>'),
(110, 17, 14, 11, 1, '_id', '17'),
(111, 17, 14, 11, 1, '_order', '1000'),
(112, 18, 15, 12, 1, 'boxtitle', 'Dealers'),
(113, 18, 15, 12, 1, 'text', '<p>Information</p>'),
(114, 18, 15, 12, 1, 'text_raw', 'Information'),
(115, 18, 15, 12, 1, 'text_processed', '<p>Information</p>'),
(116, 18, 15, 12, 1, '_id', '18'),
(117, 18, 15, 12, 1, '_order', '1000'),
(118, 19, 16, 13, 1, 'boxtitle', 'Gopher'),
(119, 19, 16, 13, 1, 'text', '<p>Information.</p>\n\n<p>(Will sort out form later)</p>'),
(120, 19, 16, 13, 1, 'text_raw', 'Information.\r\n\r\n(Will sort out form later)'),
(121, 19, 16, 13, 1, 'text_processed', '<p>Information.</p>\n\n<p>(Will sort out form later)</p>'),
(122, 19, 16, 13, 1, '_id', '19'),
(123, 19, 16, 13, 1, '_order', '1000'),
(124, 20, 17, 14, 1, 'boxtitle', 'Contacts'),
(125, 20, 17, 14, 1, 'text', '<p>Information</p>'),
(126, 20, 17, 14, 1, 'text_raw', 'Information'),
(127, 20, 17, 14, 1, 'text_processed', '<p>Information</p>'),
(128, 20, 17, 14, 1, '_id', '20'),
(129, 20, 17, 14, 1, '_order', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_content_items`
--

CREATE TABLE IF NOT EXISTS `perch2_content_items` (
  `itemRowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemID` int(10) unsigned NOT NULL DEFAULT '0',
  `regionID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `itemRev` int(10) unsigned NOT NULL DEFAULT '0',
  `itemOrder` int(10) unsigned NOT NULL DEFAULT '1000',
  `itemJSON` mediumtext NOT NULL,
  `itemSearch` mediumtext NOT NULL,
  PRIMARY KEY (`itemRowID`),
  KEY `idx_item` (`itemID`),
  KEY `idx_rev` (`itemRev`),
  KEY `idx_region` (`regionID`),
  KEY `idx_regrev` (`itemID`,`regionID`,`itemRev`),
  KEY `idx_order` (`itemOrder`),
  FULLTEXT KEY `idx_search` (`itemSearch`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `perch2_content_items`
--

INSERT INTO `perch2_content_items` (`itemRowID`, `itemID`, `regionID`, `pageID`, `itemRev`, `itemOrder`, `itemJSON`, `itemSearch`) VALUES
(1, 1, 1, 1, 0, 1000, '', ''),
(2, 1, 1, 1, 1, 1000, '{"_id":"1","text":{"raw":"This is the intro","processed":"<p>This is the intro<\\/p>"}}', ' This is the intro '),
(3, 2, 4, 2, 0, 1000, '', ''),
(4, 2, 4, 2, 1, 1000, '{"_id":"2","newstitle":"Test","_title":"Test","newstext":{"raw":"Test news","processed":"<p>Test news<\\/p>"},"date":"2013-06-30"}', ' Test Test news Sunday 30 June 2013 '),
(5, 3, 4, 2, 1, 1001, '', ''),
(6, 2, 4, 2, 2, 1000, '{"_id":"2","newstitle":"Test","_title":"Test","newstext":{"raw":"Test news","processed":"<p>Test news<\\/p>"},"date":"2013-06-30"}', ' Test Test news Sunday 30 June 2013 '),
(7, 3, 4, 2, 2, 1001, '{"_id":"3","newstitle":"Test 2","_title":"Test 2","newstext":{"raw":"Another test news","processed":"<p>Another test news<\\/p>"},"date":"2013-06-30"}', ' Test 2 Another test news Sunday 30 June 2013 '),
(8, 4, 5, 2, 0, 1000, '', ''),
(9, 4, 5, 2, 1, 1000, '{"_id":"4","newstitle":"Test","_title":"Test","newstext":{"raw":"TEst news","processed":"<p>TEst news<\\/p>"},"date":"2013-06-30"}', ' Test TEst news Sunday 30 June 2013 '),
(10, 5, 7, 4, 0, 1000, '', ''),
(11, 6, 6, 3, 0, 1000, '', ''),
(13, 7, 6, 3, 1, 1000, '', ''),
(14, 8, 19, 3, 0, 1000, '', ''),
(15, 8, 19, 3, 1, 1000, '{"_id":"8","boxtitle":"FAQ","_title":"FAQ","text":{"raw":"Content","processed":"<p>Content<\\/p>"}}', ' FAQ Content '),
(16, 9, 18, 4, 0, 1000, '', ''),
(17, 9, 18, 4, 1, 1000, '{"_id":"9","boxtitle":"Venue","_title":"Venue","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Venue Information '),
(18, 10, 8, 5, 0, 1000, '', ''),
(19, 10, 8, 5, 1, 1000, '{"_id":"10","boxtitle":"Events","_title":"Events","text":{"raw":"Intro","processed":"<p>Intro<\\/p>"}}', ' Events Intro '),
(20, 11, 8, 5, 1, 1001, '', ''),
(21, 10, 8, 5, 2, 1000, '{"_id":"10","boxtitle":"Events","_title":"Events","text":{"raw":"Intro","processed":"<p>Intro<\\/p>"}}', ' Events Intro '),
(22, 11, 8, 5, 2, 1001, '{"_id":"11","boxtitle":"An Event","_title":"An Event","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' An Event Information '),
(23, 12, 9, 6, 0, 1000, '', ''),
(24, 12, 9, 6, 1, 1000, '{"_id":"12","boxtitle":"Gaming","_title":"Gaming","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Gaming Information '),
(25, 13, 10, 7, 0, 1000, '', ''),
(26, 13, 10, 7, 1, 1000, '{"_id":"13","boxtitle":"Accommodation","_title":"Accommodation","text":{"raw":"Informatation","processed":"<p>Informatation<\\/p>"}}', ' Accommodation Informatation '),
(27, 14, 11, 8, 0, 1000, '', ''),
(28, 14, 11, 8, 1, 1000, '{"_id":"14","boxtitle":"Guests","_title":"Guests","text":{"raw":"Intro","processed":"<p>Intro<\\/p>"}}', ' Guests Intro '),
(29, 15, 12, 9, 0, 1000, '', ''),
(30, 15, 12, 9, 1, 1000, '{"_id":"15","boxtitle":"Cosplay","_title":"Cosplay","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Cosplay Information '),
(31, 16, 13, 10, 0, 1000, '', ''),
(32, 16, 13, 10, 1, 1000, '{"_id":"16","boxtitle":"Charity","_title":"Charity","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Charity Information '),
(33, 17, 14, 11, 0, 1000, '', ''),
(34, 17, 14, 11, 1, 1000, '{"_id":"17","boxtitle":"Conbook","_title":"Conbook","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Conbook Information '),
(35, 18, 15, 12, 0, 1000, '', ''),
(36, 18, 15, 12, 1, 1000, '{"_id":"18","boxtitle":"Dealers","_title":"Dealers","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Dealers Information '),
(37, 19, 16, 13, 0, 1000, '', ''),
(38, 19, 16, 13, 1, 1000, '{"_id":"19","boxtitle":"Gopher","_title":"Gopher","text":{"raw":"Information.\\r\\n\\r\\n(Will sort out form later)","processed":"<p>Information.<\\/p>\\n\\n<p>(Will sort out form later)<\\/p>"}}', ' Gopher Information.\n\n(Will sort out form later) '),
(39, 20, 17, 14, 0, 1000, '', ''),
(40, 20, 17, 14, 1, 1000, '{"_id":"20","boxtitle":"Contacts","_title":"Contacts","text":{"raw":"Information","processed":"<p>Information<\\/p>"}}', ' Contacts Information ');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_content_regions`
--

CREATE TABLE IF NOT EXISTS `perch2_content_regions` (
  `regionID` int(10) NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL,
  `regionKey` varchar(255) NOT NULL DEFAULT '',
  `regionPage` varchar(255) NOT NULL DEFAULT '',
  `regionHTML` longtext NOT NULL,
  `regionNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `regionTemplate` varchar(255) NOT NULL DEFAULT '',
  `regionMultiple` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `regionOptions` text NOT NULL,
  `regionSearchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `regionRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionLatestRev` int(10) unsigned NOT NULL DEFAULT '0',
  `regionEditRoles` varchar(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`regionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `perch2_content_regions`
--

INSERT INTO `perch2_content_regions` (`regionID`, `pageID`, `regionKey`, `regionPage`, `regionHTML`, `regionNew`, `regionOrder`, `regionTemplate`, `regionMultiple`, `regionOptions`, `regionSearchable`, `regionRev`, `regionLatestRev`, `regionEditRoles`) VALUES
(1, 1, 'Intro', '/gemucon2014/index.php', '<p>This is the intro</p>', 0, 0, 'text_block.html', 0, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(19, 3, 'Faq', '/gemucon2014/faq.php', '<div class="box">\n    <h3>FAQ</h3>\n    <p>Content</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(5, 2, 'News', '*', '<div class="w1of1 clearfix"><div class="box">\n    <h3>Test</h3>\n    <p>TEst news</p>\n    <p class="date">30 June 2013</p>\n</div></div>', 0, 0, 'newsbox.html', 1, '{"edit_mode":"singlepage","sortOrder":"DESC","sortField":"date","limit":"","searchURL":"","addToTop":"1","column_ids":"","title_delimit":"","adminOnly":0}', 1, 1, 1, '*'),
(8, 5, 'Events', '/gemucon2014/events.php', '<div class="box">\n    <h3>Events</h3>\n    <p>Intro</p>\n</div><div class="box">\n    <h3>An Event</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 2, 2, '*'),
(9, 6, 'Gaming', '/gemucon2014/gaming.php', '<div class="box">\n    <h3>Gaming</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(10, 7, 'Accommodation', '/gemucon2014/accommodation.php', '<div class="box">\n    <h3>Accommodation</h3>\n    <p>Informatation</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(11, 8, 'Guests', '/gemucon2014/guests.php', '<div class="box">\n    <h3>Guests</h3>\n    <p>Intro</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(12, 9, 'Cosplay', '/gemucon2014/cosplay.php', '<div class="box">\n    <h3>Cosplay</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(13, 10, 'Charity', '/gemucon2014/charity.php', '<div class="box">\n    <h3>Charity</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(14, 11, 'Conbook', '/gemucon2014/conbook.php', '<div class="box">\n    <h3>Conbook</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(15, 12, 'Dealers', '/gemucon2014/dealers.php', '<div class="box">\n    <h3>Dealers</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(16, 13, 'Gopher', '/gemucon2014/gopher.php', '<div class="box">\n    <h3>Gopher</h3>\n    <p>Information.</p>\n\n<p>(Will sort out form later)</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(17, 14, 'Contacts', '/gemucon2014/contacts.php', '<div class="box">\n    <h3>Contacts</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*'),
(18, 4, 'Venue', '/gemucon2014/venue.php', '<div class="box">\n    <h3>Venue</h3>\n    <p>Information</p>\n</div>', 0, 0, 'box.html', 1, '{"edit_mode":"singlepage"}', 1, 1, 1, '*');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_content_resources`
--

CREATE TABLE IF NOT EXISTS `perch2_content_resources` (
  `itemRowID` int(10) unsigned NOT NULL,
  `resourceID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`itemRowID`,`resourceID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_members`
--

CREATE TABLE IF NOT EXISTS `perch2_members` (
  `memberID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberAuthType` char(32) NOT NULL DEFAULT 'native',
  `memberAuthID` char(64) NOT NULL DEFAULT '',
  `memberEmail` char(255) NOT NULL DEFAULT '',
  `memberPassword` char(255) NOT NULL DEFAULT '',
  `memberStatus` enum('pending','active','inactive') NOT NULL DEFAULT 'pending',
  `memberCreated` datetime NOT NULL DEFAULT '2013-01-01 00:00:00',
  `memberExpires` datetime DEFAULT NULL,
  `memberProperties` text NOT NULL,
  PRIMARY KEY (`memberID`),
  KEY `idx_email` (`memberEmail`),
  KEY `idx_type` (`memberAuthType`),
  KEY `idx_active` (`memberStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_members_forms`
--

CREATE TABLE IF NOT EXISTS `perch2_members_forms` (
  `formID` int(10) NOT NULL AUTO_INCREMENT,
  `formKey` char(64) NOT NULL DEFAULT '',
  `formTitle` varchar(255) NOT NULL,
  `formSettings` text NOT NULL,
  PRIMARY KEY (`formID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_members_member_tags`
--

CREATE TABLE IF NOT EXISTS `perch2_members_member_tags` (
  `memberID` int(10) NOT NULL,
  `tagID` int(10) NOT NULL,
  `tagExpires` datetime DEFAULT NULL,
  PRIMARY KEY (`memberID`,`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_members_sessions`
--

CREATE TABLE IF NOT EXISTS `perch2_members_sessions` (
  `sessionID` char(40) NOT NULL DEFAULT '',
  `sessionExpires` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `sessionHttpFootprint` char(40) NOT NULL DEFAULT '',
  `memberID` int(10) unsigned NOT NULL DEFAULT '0',
  `sessionData` text NOT NULL,
  PRIMARY KEY (`sessionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_members_tags`
--

CREATE TABLE IF NOT EXISTS `perch2_members_tags` (
  `tagID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` char(64) NOT NULL DEFAULT '',
  `tagDisplay` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`tagID`),
  KEY `idx_tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_navigation`
--

CREATE TABLE IF NOT EXISTS `perch2_navigation` (
  `groupID` int(10) NOT NULL AUTO_INCREMENT,
  `groupTitle` varchar(255) NOT NULL DEFAULT '',
  `groupSlug` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_navigation_pages`
--

CREATE TABLE IF NOT EXISTS `perch2_navigation_pages` (
  `navpageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageID` int(10) unsigned NOT NULL DEFAULT '0',
  `groupID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL,
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`navpageID`),
  KEY `idx_group` (`groupID`),
  KEY `idx_page_group` (`pageID`,`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_pages`
--

CREATE TABLE IF NOT EXISTS `perch2_pages` (
  `pageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pageParentID` int(10) unsigned NOT NULL DEFAULT '0',
  `pagePath` varchar(255) NOT NULL DEFAULT '',
  `pageTitle` varchar(255) NOT NULL DEFAULT '',
  `pageNavText` varchar(255) NOT NULL DEFAULT '',
  `pageNew` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pageOrder` int(10) unsigned NOT NULL DEFAULT '1',
  `pageDepth` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `pageSortPath` varchar(255) NOT NULL DEFAULT '',
  `pageTreePosition` varchar(64) NOT NULL DEFAULT '',
  `pageSubpageRoles` varchar(255) NOT NULL DEFAULT '',
  `pageSubpagePath` varchar(255) NOT NULL DEFAULT '',
  `pageHidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageNavOnly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pageAccessTags` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`pageID`),
  KEY `idx_parent` (`pageParentID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `perch2_pages`
--

INSERT INTO `perch2_pages` (`pageID`, `pageParentID`, `pagePath`, `pageTitle`, `pageNavText`, `pageNew`, `pageOrder`, `pageDepth`, `pageSortPath`, `pageTreePosition`, `pageSubpageRoles`, `pageSubpagePath`, `pageHidden`, `pageNavOnly`, `pageAccessTags`) VALUES
(1, 0, '/gemucon2014/index.php', 'Gemucon2014', 'Gemucon2014', 0, 1, 1, '/gemucon2014', '000-001', '', '', 0, 0, ''),
(2, 1, '/gemucon2014/news.php', 'News', 'News', 0, 1, 2, '/gemucon2014/news', '000-001-001', '', '', 0, 0, ''),
(3, 1, '/gemucon2014/faq.php', 'Faq', 'Faq', 0, 2, 2, '/gemucon2014/faq', '000-001-002', '', '', 0, 0, ''),
(4, 1, '/gemucon2014/venue.php', 'Venue', 'Venue', 0, 3, 2, '/gemucon2014/venue', '000-001-003', '', '', 0, 0, ''),
(5, 1, '/gemucon2014/events.php', 'Events', 'Events', 0, 4, 2, '/gemucon2014/events', '000-001-004', '', '', 0, 0, ''),
(6, 1, '/gemucon2014/gaming.php', 'Gaming', 'Gaming', 0, 5, 2, '/gemucon2014/gaming', '000-001-005', '', '', 0, 0, ''),
(7, 1, '/gemucon2014/accommodation.php', 'Accommodation', 'Accommodation', 0, 6, 2, '/gemucon2014/accommodation', '000-001-006', '', '', 0, 0, ''),
(8, 1, '/gemucon2014/guests.php', 'Guests', 'Guests', 0, 7, 2, '/gemucon2014/guests', '000-001-007', '', '', 0, 0, ''),
(9, 1, '/gemucon2014/cosplay.php', 'Cosplay', 'Cosplay', 0, 8, 2, '/gemucon2014/cosplay', '000-001-008', '', '', 0, 0, ''),
(10, 1, '/gemucon2014/charity.php', 'Charity', 'Charity', 0, 9, 2, '/gemucon2014/charity', '000-001-009', '', '', 0, 0, ''),
(11, 1, '/gemucon2014/conbook.php', 'Conbook', 'Conbook', 0, 10, 2, '/gemucon2014/conbook', '000-001-010', '', '', 0, 0, ''),
(12, 1, '/gemucon2014/dealers.php', 'Dealers', 'Dealers', 0, 11, 2, '/gemucon2014/dealers', '000-001-011', '', '', 0, 0, ''),
(13, 1, '/gemucon2014/gopher.php', 'Gopher', 'Gopher', 0, 12, 2, '/gemucon2014/gopher', '000-001-012', '', '', 0, 0, ''),
(14, 1, '/gemucon2014/contacts.php', 'Contacts', 'Contacts', 0, 13, 2, '/gemucon2014/contacts', '000-001-013', '', '', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_page_templates`
--

CREATE TABLE IF NOT EXISTS `perch2_page_templates` (
  `templateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `templateTitle` varchar(255) NOT NULL DEFAULT '',
  `templatePath` varchar(255) NOT NULL DEFAULT '',
  `optionsPageID` int(10) unsigned NOT NULL DEFAULT '0',
  `templateReference` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `templateNavGroups` varchar(255) DEFAULT '',
  PRIMARY KEY (`templateID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `perch2_page_templates`
--

INSERT INTO `perch2_page_templates` (`templateID`, `templateTitle`, `templatePath`, `optionsPageID`, `templateReference`, `templateNavGroups`) VALUES
(1, 'Default', 'default.php', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_resources`
--

CREATE TABLE IF NOT EXISTS `perch2_resources` (
  `resourceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resourceApp` char(32) NOT NULL DEFAULT 'content',
  `resourceBucket` char(16) NOT NULL DEFAULT 'default',
  `resourceFile` char(255) NOT NULL DEFAULT '',
  `resourceKey` enum('orig','thumb') DEFAULT NULL,
  `resourceParentID` int(10) NOT NULL DEFAULT '0',
  `resourceType` char(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`resourceID`),
  UNIQUE KEY `idx_file` (`resourceBucket`,`resourceFile`),
  KEY `idx_app` (`resourceApp`),
  KEY `idx_key` (`resourceKey`),
  KEY `idx_type` (`resourceType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `perch2_settings`
--

CREATE TABLE IF NOT EXISTS `perch2_settings` (
  `settingID` varchar(60) NOT NULL DEFAULT '',
  `userID` int(10) unsigned NOT NULL DEFAULT '0',
  `settingValue` text NOT NULL,
  PRIMARY KEY (`settingID`,`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perch2_settings`
--

INSERT INTO `perch2_settings` (`settingID`, `userID`, `settingValue`) VALUES
('headerColour', 0, '#ffffff'),
('content_singlePageEdit', 0, '1'),
('helpURL', 0, ''),
('siteURL', 0, '/'),
('hideBranding', 0, '0'),
('content_collapseList', 0, '1'),
('lang', 0, 'en-gb'),
('update_2.2.9', 0, 'done'),
('latest_version', 0, '2.0.8');

-- --------------------------------------------------------

--
-- Table structure for table `perch2_users`
--

CREATE TABLE IF NOT EXISTS `perch2_users` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userUsername` varchar(255) NOT NULL DEFAULT '',
  `userPassword` varchar(255) NOT NULL DEFAULT '',
  `userCreated` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userLastLogin` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `userGivenName` varchar(255) NOT NULL DEFAULT '',
  `userFamilyName` varchar(255) NOT NULL DEFAULT '',
  `userEmail` varchar(255) NOT NULL DEFAULT '',
  `userEnabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userHash` char(32) NOT NULL DEFAULT '',
  `roleID` int(10) unsigned NOT NULL DEFAULT '1',
  `userMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  KEY `idx_enabled` (`userEnabled`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `perch2_users`
--

INSERT INTO `perch2_users` (`userID`, `userUsername`, `userPassword`, `userCreated`, `userUpdated`, `userLastLogin`, `userGivenName`, `userFamilyName`, `userEmail`, `userEnabled`, `userHash`, `roleID`, `userMasterAdmin`) VALUES
(1, 'rhiaro', '$P$Buo4ovDDVS1lecIV0UYn8f6fvUCVfM0', '2013-06-28 15:49:15', '2013-06-30 11:47:25', '2013-06-30 11:46:18', 'Amy', 'Guy', 'web@gemucon.co.uk', 1, '85b74a9d6c5119cf783568644016211d', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `perch2_user_privileges`
--

CREATE TABLE IF NOT EXISTS `perch2_user_privileges` (
  `privID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `privKey` varchar(255) NOT NULL DEFAULT '',
  `privTitle` varchar(255) NOT NULL DEFAULT '',
  `privOrder` int(10) unsigned NOT NULL DEFAULT '99',
  PRIMARY KEY (`privID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `perch2_user_privileges`
--

INSERT INTO `perch2_user_privileges` (`privID`, `privKey`, `privTitle`, `privOrder`) VALUES
(1, 'perch.login', 'Log in', 1),
(2, 'perch.settings', 'Change settings', 2),
(3, 'perch.users.manage', 'Manage users', 3),
(4, 'perch.updatenotices', 'View update notices', 4),
(5, 'content.regions.delete', 'Delete regions', 1),
(6, 'content.regions.options', 'Edit region options', 2),
(7, 'content.pages.edit', 'Edit page details', 1),
(8, 'content.pages.reorder', 'Reorder pages', 2),
(9, 'content.pages.create', 'Add new pages', 3),
(10, 'content.pages.configure', 'Configure page settings', 5),
(11, 'content.pages.delete', 'Delete pages', 4),
(12, 'content.templates.delete', 'Delete master pages', 6),
(13, 'content.navgroups.configure', 'Configure navigation groups', 7),
(14, 'content.navgroups.create', 'Create navigation groups', 8),
(15, 'content.navgroups.delete', 'Delete navigation groups', 9),
(16, 'perch_members', 'Manage members', 1);

-- --------------------------------------------------------

--
-- Table structure for table `perch2_user_roles`
--

CREATE TABLE IF NOT EXISTS `perch2_user_roles` (
  `roleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `roleTitle` varchar(255) NOT NULL DEFAULT '',
  `roleSlug` varchar(255) NOT NULL DEFAULT '',
  `roleMasterAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`roleID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `perch2_user_roles`
--

INSERT INTO `perch2_user_roles` (`roleID`, `roleTitle`, `roleSlug`, `roleMasterAdmin`) VALUES
(1, 'Editor', 'editor', 0),
(2, 'Admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `perch2_user_role_privileges`
--

CREATE TABLE IF NOT EXISTS `perch2_user_role_privileges` (
  `roleID` int(10) unsigned NOT NULL,
  `privID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`roleID`,`privID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perch2_user_role_privileges`
--

INSERT INTO `perch2_user_role_privileges` (`roleID`, `privID`) VALUES
(1, 1),
(1, 7),
(1, 8),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
