<div class="wrapper clearfix">
    <header class="w1of1 clearfix">
        <h1><img src="img/gemutrans900.png" alt="GemuCon 2014" /></h1>
        <h2>8 - 10 August : Warwick Arts Centre : Coventry</h2>
    </header>
</div>
<div class="w1of1 stripe clearfix">
    <div class="wrapper">
        <div class="w1of2"><p class="btnbig color2-bg light"><a href="#">Register (Soon)</a></p></div>
        <div class="w1of2"><p class="big light">to buy tickets and swag for you and your friends</p></div>
    </div>
</div>