<div class="wrapper clearfix">
    <div class="w1of1 clearfix">
        <header class="w1of2">
            <h1><img src="img/gemutrans900.png" alt="GemuCon 2014" /></h1>
        </header>
        <div class="w1of2 align-center"><div class="inner">
            <p class="btnmid color2-bg light" style="margin-top: 3.2em;"><a href="#">Register (Soon)</a></p>
            <p>Or <a href="#">login</a>, to buy tickets and swag.</p>
        </div></div>
    </div>
    <h2>8 - 10 August : Warwick Arts Centre : Coventry</h2>
</div>
<div class="stripe"></div>