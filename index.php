<? include 'top.php'; ?>
<? $pg = 'home'; ?>
<? include 'h_big.php'; ?>
<?php
    $opts = array(
        //'page'=>'/news/index.php',
        'template'=>'newsbox2.html',
        'sort'=>'date',
        'sort-order'=>'DESC',
        'count'=>2
    );
?>
	
<div class="wrapper">
    <section id="main" class="w1of1 clearfix">
        <div class="w3of4">
            <div class="box">
                <h3>GemuCon is back!</h3>
                <? perch_content('Intro'); ?>
            </div>

            <? perch_content_custom('News', $opts); ?>

        </div>
        
        <? include 'v_1of4side.php'; ?>

    </section>

<? include 'end.php' ?>

