<nav role="navigation" class="dark-bg light clearfix" id="nav">
	<ul class="top-menu wrapper">
		<li><a href="index.php">Home</a></li>
		<li><a href="news.php">News</a></li>
		<li><a href="faq.php">FAQ</a></li>
		<li><a href="venue.php">Venue</a></li>
		<li><a href="events.php">Events</a></li>
		<li><a href="gaming.php">Gaming</a></li>
		<li><a href="accommodation.php">Accommodation</a></li>
		<li><a href="guests.php">Guests</a></li>
		<li><a href="cosplay.php">Cosplay</a></li>
		<li><a href="charity.php">Charity</a></li>
		<li><a href="conbook.php">Conbook</a></li>
		<li><a class="darker-bg" href="dealers.php">Dealers</a></li>
		<li><a class="darker-bg" href="gopher.php">Gopher</a></li>
		<li><a href="contacts.php">Contacts</a></li>
		<li class="lighter color2-bg dark-bg-hov" style="float:right"><a href="/forum">Forum</a></li>
	</ul>
</nav>