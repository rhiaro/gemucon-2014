<?php

    define('PERCH_LICENSE_KEY', 'P21208-UFP059-JDQ177-RKX461-TQW028');

    define("PERCH_DB_USERNAME", 'root');
    define("PERCH_DB_PASSWORD", 'tigo');
    define("PERCH_DB_SERVER", "localhost");
    define("PERCH_DB_DATABASE", "rhiaroco_gemu14");
    define("PERCH_DB_PREFIX", "perch2_");
    
    define('PERCH_TZ', 'Europe/London');

    define('PERCH_EMAIL_FROM', 'web@gemucon.co.uk');
    define('PERCH_EMAIL_FROM_NAME', 'Amy Guy');

    define('PERCH_LOGINPATH', '/gemucon2014/perch');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', dirname(__FILE__)));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');
    
    define('PERCH_HTML5', true);
  
?>