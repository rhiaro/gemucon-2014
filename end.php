</div> <!-- end holder -->
<? include 'nav.php'; ?>
<footer class="clearfix">
</footer>
<canvas id='game'></canvas>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.2.min.js"><\/script>')</script>
<script src="js/jquery.blueberry.js"></script>
<?
$cutoff = strtotime("23:59:00 6th January 2013");
//if($regdate <= $cutoff) $cost = 35;
//else $cost = 40;
$cost = 40;
?>
<script>
$(window).load(function() {
    $('.banner').blueberry();

    // Registrations
    $('.details').hide();
    $('.trigger').click(function(){
        $(this).next('.details').toggle();
    });

    // Registration form
    // Conbag vs lanyard
    $('#form1_conbag').change(function(){
        if(this.checked){
            $('#form1_lanyard').parent('p').hide();
        }else{
            $('#form1_lanyard').parent('p').show();
        }
    });
    // Total
    var total = <?=$cost?>;
    $('#form1_conbag').change(function(){
        if($(this).attr('checked') == 'checked'){
            total += 10;
            //alert($('#form1_lanyard').attr('checked'));
            if($('#form1_lanyard').attr('checked') == 'checked'){
                total -= 3;
                $('#total').text(total);
                $('#form1_lanyard').attr('checked', false).uniform();
            }
            $('#total').text(total);
        }else{
            total -= 10;
            $('#total').text(total);
        }
        $('#total').text(total);
    });
    $('#form1_lanyard').change(function(){
        if(this.checked){
            total += 3;
        }else{
            total -= 3;
        }
        $('#total').text(total);
    });
    var val = "None";
    $('#form1_shirt').change(function(){
        if(val == "None" && $(this).val() != "None"){
            total += 10;
            val = "Y";
        }
        if(val != "None" && $(this).val() == "None"){
            total -= 10;
            val = "None";
        }
        $('#total').text(total);
    });
});
</script>
<script src="js/tetris.js"></script>
<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

</body>
</html>