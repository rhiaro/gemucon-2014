<?
require_once('perch/runtime.php');
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>GemuCon 2014</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">

	<script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js"></script>
	<link href='http://fonts.googleapis.com/css?family=Iceberg' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Forum' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>

	<!--[if lt IE 9]>
		<style type="text/css">
			a, a:visted { color: #fff; }
		</style>
	<![endif]-->

	<!--[if gte IE 9]>
	  <style type="text/css">
	    body {
	       filter: none;
	    }
	  </style>
	<![endif]-->
</head>
<body>
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
<nav class="navlink dark-bg light clearfix"><ul><li><a href="#nav">Menu</a></li></ul></nav>
